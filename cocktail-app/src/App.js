import { SearchCocktails } from './components';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/cocktails" element={<SearchCocktails />} />
        <Route path="/repas" element={<SearchCocktails apiUrl="www.themealdb.com/api/json/v1/1/" type='repas'/>} />
        <Route path="*" element={<>
          <h1>404</h1>
          <Link to="/cocktails">
            App cocktail
          </Link>
        </>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;