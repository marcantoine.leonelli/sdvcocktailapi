import styles from "./FormFilters.module.css";
import { useState, useEffect } from "react";

const FormFilters = ({onSubmit}) => {
  const [categories, setCategories] = useState([]);
  const [glasses, setGlasses] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [alcoholic, setAlcoholic] = useState([]);

  const [categoriesSelected, setCategoriesSelected] = useState(null);
  const [glassesSelected, setGlassesSelected] = useState(null);
  const [ingredientsSelected, setIngredientsSelected] = useState(null);
  const [alcoholicSelected, setAlcoholicSelected] = useState(null);

  const getFiltersList = async (filter) => {
    let response = await fetch(
      `https://www.thecocktaildb.com/api/json/v1/1/list.php?${filter}=list`
    );
    response = await response.json();
    return response.drinks;
  };

  useEffect(() => {
    getFiltersList("c").then((categories) => setCategories(categories));
    getFiltersList("g").then((glasses) => setGlasses(glasses));
    getFiltersList("i").then((ingredients) => setIngredients(ingredients));
    getFiltersList("a").then((alcoholic) => setAlcoholic(alcoholic));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const filter = {
      c: categoriesSelected,
      g: glassesSelected,
      i: ingredientsSelected,
      a: alcoholicSelected,
    };

    onSubmit(filter);
  };

  const handleSelectChange = (setFunction, value) => {
    if (value) {
      setFunction(value);
    } else {
      setFunction('');
    }
  }

  return (
    <form action="" id={styles.FormFilters} onSubmit={handleSubmit}>
      <select onChange={(e) => handleSelectChange(setCategoriesSelected, e.target.value)}>
        <option value=''>Catégorie</option>
        {categories.map((category, index) => (
          <option key={index} value={category.strCategory}>
            {category.strCategory}
          </option>
        ))}
      </select>
      <select onChange={(e) => handleSelectChange(setGlassesSelected, e.target.value)}>
        <option value=''>Type de verre</option>
        {glasses.map((glasse, index) => (
          <option key={index} value={glasse.strGlass}>
            {glasse.strGlass}
          </option>
        ))}
      </select>
      <input type="text" list="ingredients" placeholder="Ingrédients" onChange={(e) => handleSelectChange(setIngredientsSelected, e.target.value)}/>
      <datalist id="ingredients">
        {ingredients.map((ingredient, index) => (
          <option key={index} value={ingredient.strIngredient1}>
            {ingredient.strIngredient1}
          </option>
        ))}
      </datalist>
      <select onChange={(e) => handleSelectChange(setIngredientsSelected, e.target.value)}>
        <option value=''>Ingrédients</option>
        {ingredients.map((ingredient, index) => (
          <option key={index} value={ingredient.strIngredient1}>
            {ingredient.strIngredient1}
          </option>
        ))}
      </select>
      <select onChange={(e) => handleSelectChange(setAlcoholicSelected, e.target.value)}>
        <option value=''>Avec/sans alcool</option>
        {alcoholic.map((type, index) => (
          <option key={index} value={type.strAlcoholic}>
            {type.strAlcoholic}
          </option>
        ))}
      </select>
      <input type="submit" value="Rechercher" />
    </form>
  );
};

export default FormFilters;
