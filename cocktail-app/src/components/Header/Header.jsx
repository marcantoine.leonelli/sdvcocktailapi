import styles from './Header.module.css';

const Header = ({onChangeInput, valueInput, type}) => {
  const handleInputChange = (e) => {
    onChangeInput(e.target.value);
  }

  return (
    <header className={styles.header}>
      <p>Rechercher des {type}</p>
      <input type="text" onChange={handleInputChange} value={valueInput}/>
    </header>
  )
}

export default Header;