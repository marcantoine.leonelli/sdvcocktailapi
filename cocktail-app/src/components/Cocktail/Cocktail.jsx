import { useState } from 'react';
import styles from './Cocktail.module.css';
import { Modal } from '..';

const Cocktail = ({cocktail}) => {
  const [showModal, setShowModal] = useState(false);

  const [cocktailInfos, setCocktailInfos] = useState(cocktail);

  const ingredients = [];
  for (let i = 1; i < 16; i++) {
    const ingredient = cocktailInfos["strIngredient" + i];

    if (ingredient) {
      ingredients.push(ingredient);
    }
  }

  const handleCocktailClick = async () => {
    setShowModal(true);
    const cocktailAttributes = Object.keys(cocktail).filter(key => cocktail[key]);
    if (cocktailAttributes.length < 4) {
      let response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${cocktail.idDrink}`);
      response = await response.json();

      setCocktailInfos(response.drinks[0]);
      setShowModal(true);
    }
  }

  return (
    <>
      <div className={styles.cocktail} onClick={handleCocktailClick}>
        <h2>{cocktailInfos.strDrink}</h2>
        <img className={styles.cocktailThumb} src={cocktailInfos.strDrinkThumb}/>
        <div className={styles.ingredientsContainer}>
          {ingredients.map((ingredient, index) => (
            <span key={index} className={styles.ingredient}>{ingredient}</span>
          ))}
        </div>
      </div>
      {showModal && (
        <Modal onClose={() => setShowModal(false)}>
          <>
          <h2>{cocktailInfos.strDrink}</h2>
          <img className={styles.cocktailThumb} src={cocktailInfos.strDrinkThumb}/>
          <div className={styles.ingredientsContainer}>
            {ingredients.map((ingredient, index) => (
              <span key={index} className={styles.ingredient}>{ingredient}</span>
            ))}
          </div>
          <p>{cocktailInfos.strInstructions}</p>
          </>
        </Modal>
      )}
    </>
  )
}

export default Cocktail;