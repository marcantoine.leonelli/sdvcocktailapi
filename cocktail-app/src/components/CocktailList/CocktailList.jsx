import { Cocktail } from '..';
import styles from './CocktailList.module.css';

const CocktailList = ({cocktails}) => {
  return (
    <div className={styles.container}>
      {cocktails.map((cocktail, index) => (
        <Cocktail key={cocktail.idDrink} cocktail={cocktail}/>
      ))}
    </div>
  )
}

export default CocktailList