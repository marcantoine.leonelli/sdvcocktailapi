import { Header, Footer, CocktailList, FormFilters } from '..'
import React ,{ useState, useEffect } from 'react';

const SearchCocktails = ({apiUrl = "www.thecocktaildb.com/api/json/v1/1/", type = "cocktails"}) => {
  const [cocktails, setCocktails] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fetch(`https://${apiUrl}search.php?s=${search}`)
      .then((response) => response.json())
      .then((response) => {
        if (response.drinks) {
          setCocktails(response.drinks);
        } else {
          setCocktails([]);
        }
      })
  }, [search, apiUrl]);

  const handleFiltersSubmit = (filters) => {
    const validFilters = Object.keys(filters).filter(key => filters[key]);
    if (validFilters.length > 0) {
      const fetchUrl = `'https://${apiUrl}filter.php?'` + validFilters.map(key => key + '=' + filters[key].replace(/ /g, '_')).join('&');
      setSearch('');
      fetch(fetchUrl)
        .then((response) => response.json())
        .then((response) => {
          if (response.drinks) {
            setCocktails(response.drinks);
          } else {
            setCocktails([]);
          }
        });
    }
  };

  return (
    <div>
      <Header onChangeInput={setSearch} valueInput={search} type={type}></Header>
      <main>
        <p>Voici la liste des {type}</p>
        <FormFilters onSubmit={handleFiltersSubmit}/>
        <CocktailList cocktails={cocktails}/>
      </main>
      <Footer></Footer>
    </div>
  );
}

export default SearchCocktails;
