import styles from './Modal.module.css';
import { useEffect, useRef } from 'react';

/**
 * Hook that alerts clicks outside of the passed ref
 */
function useOutsideAlerter(ref, onClose) {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        onClose();
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
}


const Modal = ({children, onClose}) => {
  const modalRef = useRef(null);
  useOutsideAlerter(modalRef, onClose);

  return (
    <div id={styles.ModalContainer}>
      <div id={styles.ModalBack}>
        <div ref={modalRef} id={styles.Modal}>
          {children}
        </div>
      </div>
    </div>
  )
}

export default Modal;