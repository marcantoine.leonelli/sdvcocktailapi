const init = () => {
  new App();
}

class App {
  constructor() {
    this.root = document.getElementById("root");
    this.button = new Button("click me", this.onClickButton);
    this.container = document.createElement("div");
    this.root.appendChild(this.button.render());
    this.root.appendChild(this.container);

    this.fetchDrinks();

    this.onClickButton = this.onClickButton.bind(this);
    this.clearContainer = this.clearContainer.bind(this);
    this.render = this.render.bind(this);
  }

  onClickButton() {
    this.clearContainer();
    this.render();
  }

  async fetchDrinks() {
    let response = await fetch(
      "https://www.thecocktaildb.com/api/json/v1/1/search.php?s="
    );
    response = await response.json();
    this.drinks = response.drinks;

    this.render();
  }

  clearContainer() {
    this.container.innerHTML = "";
  }

  render() {
    this.clearContainer();

    for (let drink of this.drinks) {
      const cocktail = new Cocktail(drink);
      this.container.appendChild(cocktail.render());
    }
  }
}

class Button {
  constructor (label, onClick) {
    this.label = label;
    this.onClick = onClick;
  }

  render() {
    const button = document.createElement("button");
    button.textContent = this.label;
    button.addEventListener("click", this.onClick);

    return button;
  }
}

class Cocktail {
  constructor(drink) {
    this.drink = drink;
  }

  addElement(parent, elementName, content = '') {
    const element = document.createElement(elementName);
    element.textContent = content;
    parent.appendChild(element);

    return element;
  };

  render() {
    const cocktailElement = document.createElement("div");
    const drink = this.drink;

    this.addElement(cocktailElement, "h1", drink.strDrink);

    this.addElement(cocktailElement, "p", "catégorie : " + drink.strCategory);
    this.addElement(cocktailElement, "p", "instructions : " + drink.strInstructions);

    const ul = this.addElement(cocktailElement, "ul");

    for (let i = 1; i < 16; i++) {
      const ingredient = drink["strIngredient" + i];

      if (ingredient) {
        this.addElement(ul, "li", ingredient);
      }
    }

    const img = document.createElement("img");
    img.src = drink.strDrinkThumb;
    cocktailElement.appendChild(img);

    return cocktailElement;
  }
}