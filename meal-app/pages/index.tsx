import { Header, Footer, RandomMealShow, LargeMealShow, MealShow } from '../components';
import { Meal } from '../comon/types/types';
import { useEffect, useState } from 'react';
import styles from '../styles/Home.module.scss';
import Link from 'next/link';

export default function Home() {
  const [meals, setMeals] = useState<Meal[]>([]);
  const [randomMealKey, setRandomMealKey] = useState<number>(0);

  useEffect(() => {
    (async () => {
      const response = await fetch('https://www.themealdb.com/api/json/v1/1/search.php?s=');
      const data = await response.json();
      if (data.meals) {
        setMeals(data.meals.slice(0, 3));
      }
    })();
  }, []);

  return (
    <>
      <Header />
      <main>
        <h1>Accueil</h1>
        <div className={styles.titleContainer}>
          <h2>Plat aléatoire</h2>
          <span onClick={() => setRandomMealKey(randomMealKey + 1)}>générer un nouveau</span>
        </div>
        <RandomMealShow key={randomMealKey}/>
        <div className={styles.titleContainer}>
          <h2>Les plats</h2>
          <Link href={'/meals'}><span>Voir tous les plats</span></Link>
        </div>
        <div className={styles.mealsContainer}>
          {meals.map((meal) => (
            <div key={meal.idMeal}>
              <LargeMealShow meal={meal} />
            </div>
          ))}
        </div>
      </main>
      <Footer />
    </>
  )
}
