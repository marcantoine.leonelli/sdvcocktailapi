import { useEffect, useState } from "react";
import { Meal } from "../../comon/types/types";
import { Header, Footer, LargeMealShow, SearchInput } from "../../components";
import styles from "../../styles/Meals.module.scss";
import { useRouter } from "next/router";


export default function Meals() {
  const [meals, setMeals] = useState<Meal[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  const router = useRouter();
  const { search } = router.query;

  useEffect(() => {
    (async () => {
      setLoading(true);
      let fetchUrl;
      if (search) {
        fetchUrl = `https://www.themealdb.com/api/json/v1/1/search.php?s=${search}`;
      } else {
        fetchUrl = `https://www.themealdb.com/api/json/v1/1/search.php?s=`;
      }
      const response = await fetch(fetchUrl);
      const data = await response.json();
      if (data.meals) {
        setMeals(data.meals);
      } else {
        //setError("Aucun résultat");
      }
      setLoading(false);
    })();
  }, [search]);

  return (
    <>
      <Header />
      <main>
        <h1>Plats</h1>
        {search && <p>Recherche : {search}</p>}
        {loading ? (
          <p>Chargement...</p>
        ) : error ? (
          <p>{error}</p>
        ) : (
          <div className={styles.mealsContainer}>
            {meals.map((meal) => (
              <LargeMealShow key={meal.idMeal} meal={meal} />
            ))}
          </div>
        )}
      </main>
      <Footer />
    </>
  );
}