import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Meal } from "../../comon/types/types";
import { Header, Footer, LargeMealShow } from "../../components";
import styles from "../../styles/MealPage.module.scss";

export default function MealPage() {
  const router = useRouter();
  const { id } = router.query;

  const [meal, setMeal] = useState<Meal | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  useEffect(() => {
    (async () => {
      setLoading(true);
      const response = await fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${id}`);
      const data = await response.json();
      if (data.meals) {
        setMeal(data.meals[0]);
        setError("");
      } else {
        setError("Aucun résultat");
      }
      setLoading(false);
    })();
  }, [id]);

  const ingredients = [];
  if (meal !== null) {
    const anyMeal: any = meal;
    for (let i = 1; i <= 20; i++) {
      if (anyMeal[`strIngredient${i}`]) {
        ingredients.push(anyMeal[`strIngredient${i}`]);
      }
    }
  }

  return (
    <>
      <Header />
      <main>
        {loading ? (
          <p>Chargement...</p>
        ) : error ? (
          <p>{error}</p>
        ) : (
          <div>
            {meal && (
              <>
                <div className={styles.header}>
                  <h1>{meal.strMeal}</h1>
                  <span className={styles.category}>{meal.strCategory}</span>
                  {meal.strTags && (
                    <span className={styles.tags}>{meal.strTags}</span>
                  )}
                </div>
                <img className={styles.image} src={meal.strMealThumb} alt={meal.strMeal} />
                <ul className={styles.ingredientsContainer}>
                  {ingredients.map((ingredient, index) => (
                    <li key={index}>{ingredient}</li>
                  ))}
                </ul>
                <p className={styles.instructions}>{meal.strInstructions}</p>
              </>
            )}
          </div>
        )}
      </main>
      <Footer />
    </>
  );
}