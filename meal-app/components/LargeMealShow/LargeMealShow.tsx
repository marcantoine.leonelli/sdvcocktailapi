import { Meal } from "../../comon/types/types";
import Link from "next/link";
import styles from "./LargeMealShow.module.scss";

type LargeMealShowProps = {
  meal: Meal;
};

export default function LargeMealShow({ meal }: LargeMealShowProps) {
  const ingredients = [];
  if (meal !== null) {
    const anyMeal: any = meal;
    for (let i = 1; i <= 20; i++) {
      if (anyMeal[`strIngredient${i}`]) {
        ingredients.push(anyMeal[`strIngredient${i}`]);
      }
    }
  }

  return (
    <div id={styles.LargeMealShow}>
      {meal && (
        <>
          <div className={styles.header}>
            <h3>
              <Link href={`/meal/${meal.idMeal}`}>{meal.strMeal}</Link>
            </h3>
            <span className={styles.category}>{meal.strCategory}</span>
            {meal.strTags && (
              <span className={styles.tags}>{meal.strTags}</span>
            )}
          </div>
          <div className={styles.body}>
            <img src={meal.strMealThumb} alt={meal.strMeal} />
            <div className={styles.description}>
              <ul className={styles.ingredientsContainer}>
                {ingredients.map((ingredient, index) => (
                  <li key={index}>{ingredient}</li>
                ))}
              </ul>
              <p className={styles.instructions}>{meal.strInstructions}</p>
              <Link className={styles.link} href={`/meal/${meal.idMeal}`}>
                ... Voir plus {">"}
              </Link>
            </div>
          </div>
        </>
      )}
    </div>
  );
}
