import Link from "next/link";
import styles from "./Header.module.scss";
import { SearchInput } from '..';
import { useState } from "react";
import { useRouter } from "next/router";

export default function Header () {
  const [search, setSearch] = useState<string>("");

  const router = useRouter();

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    router.push(`/meals/${search}`);
  };

  return (
    <header id={styles.Header}>
      <h1>Repas</h1>
      <form onSubmit={handleSubmit}>
        <SearchInput onChange={handleInputChange} value={search}/>
      </form>
      <nav>
        <ul>
          <li>
            <Link href="/">Accueil</Link>
          </li>
          <li>
            <Link href="/meals">Plats</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};