import styles from './SearchInput.module.scss';

type SearchInputProps = {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
};

export default function SearchInput({onChange, value}: SearchInputProps) {
  return (
    <input
      className={styles.searchInput}
      type="text"
      placeholder="Rechercher un plat"
      onChange={onChange}
      value={value}
    />
  );
}