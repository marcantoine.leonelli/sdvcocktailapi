import React, { useState, useEffect } from 'react';
import { Meal } from '../../comon/types/types';
import Link from 'next/link';
import { LargeMealShow } from '..';

export default function RandomMealShow () {
  const [meal, setMeal] = useState<Meal | null>(null);

  useEffect(() => {
    (async () => {
      const response = await fetch('https://www.themealdb.com/api/json/v1/1/random.php');
      const data = await response.json();
      if (data.meals[0]) {
        setMeal(data.meals[0]);
      }
    })();
  }, []);

  return (
    <div>
      {meal && (
        <LargeMealShow meal={meal} />
      )}
    </div>
  )
}